package maosui.com.bsdiff;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView textView = (TextView) findViewById(R.id.textView1);

        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/qt/bsdiff";
        String oldPath = path + "/v2.1.0-3233.zip";
        String newSavePath = path + "/v2.1.0-3234.zip";
        String patchPath = path + "/v2.1.0-3233_v2.1.0-3234.patch";

        int sts = BSDiff.applyPatch(
                oldPath,
                newSavePath,
                patchPath
        );

        String resultText = "apply patch: \n" +
                oldPath + "\n" +
                newSavePath + "\n" +
                patchPath + "\n" +
                (sts == 0 ? "success. file size:" + new File(newSavePath).length() : "fail. status: " + sts);

        textView.setText(resultText);
    }
}
