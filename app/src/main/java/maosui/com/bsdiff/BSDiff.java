package maosui.com.bsdiff;

public class BSDiff {

    static {
        System.loadLibrary("bsdiff");
    }

    /**
     * 应用差异包
     * @param oldPath 旧版本文件路径,文件必须存在
     * @param newPath 新版本文件保存路径
     * @param patchPath 差异包路径,文件必须存在
     * @return 状态,0 表示成功
     */
    public native static int applyPatch(String oldPath, String newPath, String patchPath);
}
