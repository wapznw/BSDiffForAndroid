
#ifndef __BS_PATCH__
#define __BS_PATCH__

#ifdef __cplusplus
extern "C" {
#endif

int applyPatch(const char *old_path, const char *new_path, const char *patch_path);

#ifdef __cplusplus
}
#endif

#endif