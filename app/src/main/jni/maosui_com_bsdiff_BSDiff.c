#include "maosui_com_bsdiff_BSDiff.h"
#include <android/log.h>

#include "bspatch.h"

#define LOG_INFO(...) __android_log_print(ANDROID_LOG_INFO, "JNI_TAG", __VA_ARGS__)

jint JNICALL Java_maosui_com_bsdiff_BSDiff_applyPatch(JNIEnv *env, jclass cls, jstring oldPath,
                                                      jstring newPath, jstring patchPath) {

    const char *old = (*env)->GetStringUTFChars(env, oldPath, JNI_FALSE);
    const char *new = (*env)->GetStringUTFChars(env, newPath, JNI_FALSE);
    const char *patch = (*env)->GetStringUTFChars(env, patchPath, JNI_FALSE);

    LOG_INFO("old: %s, new: %s, patch: %s", old, new, patch);

    int sts = applyPatch(old, new, patch);

    LOG_INFO("applyPatch status: %d", sts);

    (*env)->ReleaseStringUTFChars(env, oldPath, old);
    (*env)->ReleaseStringUTFChars(env, newPath, new);
    (*env)->ReleaseStringUTFChars(env, patchPath, patch);

    return sts;
}
